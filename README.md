# Advisor 360 CDN Tech Challenge

Currently front end feature development is hard to do without impacting groups of people due to being limited to testing one version of an application at a time. 

Design a cloud based CDN solution to accommodate a multi-tenant / multi-env platform under a shared CDN. 

Need to allow developers to test main branches and feature branches in parallel. Relative routing to applications should remain consistent.

Static content might call backend micro services that require to be deployed on containers.

How can build the above in testable way (prod/nonprod/feature branches name spaces)?

How can the static files be edge optimized?

How can you troubleshoot a case of 404 error?

How can you prevent errors to be displayed to end users and use friendly error page that is tenant specific?

### My approach:

- For this challenge, I chose to deploy an Azure Web App to serve a static html page and an Azure CDN to deliver the content faster and more reliably by using endpoints that are closet to the users. This increases speed and availability, resulting in an improved user experience. All updates to the Web App are made via git using the BitBucket repository. 

[nicecloudapp](https://nicecloudapp.azurewebsites.net/)

### Resources used for this effort:

- [Azure Subscription](https://portal.azure.com)
- [BitBucket Project Repository](https://bitbucket.org/steve727/cdn)
- [Azure Web Apps](https://azure.microsoft.com/en-us/services/app-service/web/)
- [Azure App Service QuickStart](https://docs.microsoft.com/en-us/azure/app-service/quickstart-html)
- [Tutorial: Add Azure CDN to an Azure App Service web app](https://docs.microsoft.com/en-us/azure/cdn/cdn-add-to-web-app)
- [Tutorial: Configure HTTPS on an Azure CDN custom domain](https://docs.microsoft.com/en-us/azure/cdn/cdn-custom-ssl?tabs=option-1-default-enable-https-with-a-cdn-managed-certificate)

### Steps that I took to get here:

- Created a free Azure account
- Created a free BitBucket account
- Configured BitBucket access keys
- Built an Azure Web App to serve a static html page
- Created an Azure CDN profile and endpoint settings on the App Service
- Configured HTTPS on an Azure CDN custom domain [thenice.cloud](https://thenice.cloud)

### Development App Service Slot
https://nicecloudapp-dev.azurewebsites.net

### Production Web App
https://www.thenice.cloud

https://nicecloudapp.azurewebsites.net/

### CDN Endpoint
https://nicecloudapp.azureedge.net/

