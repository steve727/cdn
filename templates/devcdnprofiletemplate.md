```json
{
    "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
        "profiles_a360cdnprofile_name": {
            "defaultValue": "a360cdnprofiletemplate",
            "type": "String"
        }
    },
    "variables": {},
    "resources": [
        {
            "type": "Microsoft.Cdn/profiles/endpoints",
            "apiVersion": "2021-06-01",
            "name": "[concat(parameters('profiles_a360cdnprofile_name'), '/nicecloudappdev')]",
            "location": "Global",
            "properties": {
                "originHostHeader": "nicecloudappdev.azurewebsites.net",
                "originPath": "/Dev",
                "contentTypesToCompress": [
                    "application/eot",
                    "application/font",
                    "application/font-sfnt",
                    "application/javascript",
                    "application/json",
                    "application/opentype",
                    "application/otf",
                    "application/pkcs7-mime",
                    "application/truetype",
                    "application/ttf",
                    "application/vnd.ms-fontobject",
                    "application/xhtml+xml",
                    "application/xml",
                    "application/xml+rss",
                    "application/x-font-opentype",
                    "application/x-font-truetype",
                    "application/x-font-ttf",
                    "application/x-httpd-cgi",
                    "application/x-javascript",
                    "application/x-mpegurl",
                    "application/x-opentype",
                    "application/x-otf",
                    "application/x-perl",
                    "application/x-ttf",
                    "font/eot",
                    "font/ttf",
                    "font/otf",
                    "font/opentype",
                    "image/svg+xml",
                    "text/css",
                    "text/csv",
                    "text/html",
                    "text/javascript",
                    "text/js",
                    "text/plain",
                    "text/richtext",
                    "text/tab-separated-values",
                    "text/xml",
                    "text/x-script",
                    "text/x-component",
                    "text/x-java-source"
                ],
                "isCompressionEnabled": true,
                "isHttpAllowed": false,
                "isHttpsAllowed": true,
                "queryStringCachingBehavior": "IgnoreQueryString",
                "origins": [
                    {
                        "name": "nicecloudappdev-azurewebsites-net",
                        "properties": {
                            "hostName": "nicecloudappdev.azurewebsites.net",
                            "httpPort": 80,
                            "httpsPort": 443,
                            "originHostHeader": "nicecloudappdev.azurewebsites.net",
                            "priority": 1,
                            "weight": 1000,
                            "enabled": true
                        }
                    }
                ],
                "originGroups": [],
                "geoFilters": [
                    {
                        "relativePath": "/",
                        "action": "Block",
                        "countryCodes": [
                            "RU"
                        ]
                    }
                ]
            }
        },
        {
            "type": "Microsoft.Cdn/profiles/endpoints/customdomains",
            "apiVersion": "2021-06-01",
            "name": "[concat(parameters('profiles_a360cdnprofile_name'), '/nicecloudapp/dev-thenice-cloud')]",
            "dependsOn": [
                "[resourceId('Microsoft.Cdn/profiles/endpoints', parameters('profiles_a360cdnprofile_name'), 'nicecloudappdev')]"
            ],
            "properties": {
                "hostName": "dev.thenice.cloud"
            }
        },
        {
            "type": "Microsoft.Cdn/profiles/endpoints/origins",
            "apiVersion": "2021-06-01",
            "name": "[concat(parameters('profiles_a360cdnprofile_name'), '/nicecloudapp/nicecloudappdev-azurewebsites-net')]",
            "dependsOn": [
                "[resourceId('Microsoft.Cdn/profiles/endpoints', parameters('profiles_a360cdnprofile_name'), 'nicecloudappdev')]"
            ],
            "properties": {
                "hostName": "nicecloudappdev.azurewebsites.net",
                "httpPort": 80,
                "httpsPort": 443,
                "originHostHeader": "nicecloudappdev.azurewebsites.net",
                "priority": 1,
                "weight": 1000,
                "enabled": true
            }
        }
    ]
}
```